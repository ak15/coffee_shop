class LineItem < ApplicationRecord
  belongs_to :order
  belongs_to :product

  before_save :calculate_price

  after_commit :update_order

  before_commit :verify_bulk_associated_products

  def calculate_price
    self.amount = product.caluclate_price_of_one_item(self) * quantity
  end

  def sorted_product_ids_in_order
    order.line_items.where.not(product_id: product_id).order(:product_id).pluck(:product_id)
  end

  def update_order
    order.update(total_price: order.line_items.sum(:amount))
  end

  def verify_bulk_associated_products
    order.line_items.each do |item|
      # break condition to infinite loop
      next if product_id == item.product_id

      item.update(amount: item.calculate_price)
    end
  end
end
