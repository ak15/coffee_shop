class Payment < ApplicationRecord
  belongs_to :order, optional: true
  belongs_to :customer, optional: true

  enum payment_gateway: { amazon: 1, stripe: 2 }
end
