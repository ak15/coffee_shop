class Order < ApplicationRecord
  belongs_to :customer
  has_many :payments
  has_many :line_items

  enum status: { inprogress: 1, completed: 2, cancelled: 3, locked: 4, refunded: 5 }

  WAIT_TIME = 5

  def add(product_id)
    return unless inprogress?

    line_item = line_items.find_by(product_id: product_id)
    if line_item.present?
      line_item.update(quantity: line_item.quantity + 1)
    else
      line_items.create(product_id: product_id)
    end
  end

  def send_success_notification
    customer.notifications.create(title: "Order #{id}", description: "Order #{id} has been completed")
  end
end
