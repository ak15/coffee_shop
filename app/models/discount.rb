class Discount < ApplicationRecord
  belongs_to :product
  has_many :discount_groups, dependent: :destroy

  enum category: { fixed_percentage: 1, free: 2, bulk: 3 }

  has_many :discount_groups, dependent: :nullify

  def calculate_price(line_item)
    unit_price = if bulk?
                   single_unit_price_after_bulk_discount(line_item)
                 else
                   send "single_unit_price_after_#{category}_discount"
                 end
    product.total_price(unit_price)
  end

  def single_unit_price_after_fixed_percentage_discount
    # actual discount is never on tax
    (product.price - (product.price * discount / 100)).abs
  end

  def single_unit_price_after_free_discount
    0
  end

  def single_unit_price_after_bulk_discount(line_item)
    if line_item.sorted_product_ids_in_order == discount_groups.order(:product_id).pluck(:product_id)
      single_unit_price_after_fixed_percentage_discount
    else
      product.price
    end
  end
end
