class Product < ApplicationRecord
  has_one :discount, dependent: :destroy
  validates :name, presence: true, uniqueness: { case_sensitive: false }

  def caluclate_price_of_one_item(line_item)
    return total_price(price) if discount.blank?

    discount.calculate_price(line_item)
  end

  def total_price(single_unit_price)
    single_unit_price + calculate_single_unit_tax_amount(single_unit_price)
  end

  def calculate_single_unit_tax_amount(single_unit_price)
    (single_unit_price * tax / 100)
  end
end
