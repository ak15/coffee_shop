class Customer < ApplicationRecord
  has_many :orders, dependent: :nullify
  has_many :payments, dependent: :nullify
  has_many :notifications, dependent: :destroy
end
