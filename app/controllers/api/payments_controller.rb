class Api::PaymentsController < Api::BaseController
  # can add authentication and authorization later in futher improvements

  # we can find current_customer but now will use only params

  def create
    order = Order.inprogress.find_by(id: params[:order_id])
    if order
      order.payments.create(customer_id: order.customer.id, payment_gateway: :stripe)
      # we can create mock service and say payment success
      order.completed!
      Concurrent::ScheduledTask.new(Order::WAIT_TIME) do
        order.send_success_notification
      end
      render json: { order: order }, status: :ok
    else
      render json: { error: 'Something went wrong' }, status: :unprocessable_entity
    end
  end
end
