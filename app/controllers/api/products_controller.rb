class Api::ProductsController < Api::BaseController
  # can add authentication and authorization later in futher improvements
  def index
    products = Product.all
    # could implement pagination, searching etc
    respond_to do |format|
      format.json { render json: products, status: :ok }
    end
  end
end
