class Api::LineItemsController < Api::BaseController
  # can add authentication and authorization later in futher improvements

  # we can find current_customer but now will use only params

  def create
    order = Order.find_or_create_by(customer_id: params[:customer_id], status: :inprogress)
    if order
      order.add(params[:product_id])
      render json: { order: order }, status: :ok
    else
      render json: { error: 'Something went wrong' }, status: :unprocessable_entity
    end
  end
end
