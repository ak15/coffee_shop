FactoryBot.define do
  factory :notification do
    title { "MyString" }
    description { "MyString" }
    user { nil }
  end
end
