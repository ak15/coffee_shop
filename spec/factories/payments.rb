FactoryBot.define do
  factory :payment do
    order { nil }
    customer { nil }
    payment_gateway { 1 }
  end
end
