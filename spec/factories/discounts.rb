FactoryBot.define do
  factory :discount do
    product { nil }
    discount { "9.99" }
  end
end
