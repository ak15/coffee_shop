FactoryBot.define do
  factory :order do
    product { nil }
    quantity { 1 }
    price { "9.99" }
    customer { nil }
  end
end
