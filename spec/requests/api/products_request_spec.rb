require 'rails_helper'

RSpec.describe 'Api::Products', type: :request do
  path '/api/products' do
    get 'List Products' do
      tags 'Products'
      consumes 'application/json'
      # security [bearerAuth: []]

      # params

      response '200', 'List Products' do
        # example
        run_test!
      end
    end
  end
end
