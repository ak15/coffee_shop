require 'rails_helper'

RSpec.describe 'Api::LineItems', type: :request do
  path '/api/line_items' do
    post 'Creates a line_item' do
      tags 'LineItems'
      consumes 'application/json'
      # security [bearerAuth: []]

      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          customer_id: { type: :integer },
          product_id: { type: :integer }
        }
      }

      response '200', 'line_item created' do
        run_test!
      end
    end
  end
end
