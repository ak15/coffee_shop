# typed: strict

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

%w[Tea Coffee Cookies Milk Bread Water].each do |name|
  Product.find_or_create_by(name: name, price: 100, tax: 0)
end

Discount.find_or_create_by(product_id: Product.find_by(name: 'Milk').id, category: :fixed_percentage, discount: 10)
Discount.find_or_create_by(product_id: Product.find_by(name: 'Cookies').id, category: :free, discount: 100)
discount = Discount.find_or_create_by(product_id: Product.find_by(name: 'Tea').id, category: :bulk, discount: 5)

discount.discount_groups.find_or_create_by(product_id: Product.find_by(name: 'Cookies').id)
discount.discount_groups.find_or_create_by(product_id: Product.find_by(name: 'Milk').id)

customer = Customer.find_or_create_by(name: 'John Doe', email: 'testjohndoe@test.com')
order = Order.find_or_create_by(customer_id: customer.id)
order.add(Product.find_by(name: 'Milk').id)

order.add(Product.find_by(name: 'Cookies').id)
order.add(Product.find_by(name: 'Tea').id)
order.add(Product.find_by(name: 'Bread').id)
