class CreateNotifications < ActiveRecord::Migration[7.0]
  def change
    create_table :notifications do |t|
      t.string :title, null: false
      t.string :description, null: false
      t.references :customer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
