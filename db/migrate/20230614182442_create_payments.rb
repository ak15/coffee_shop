class CreatePayments < ActiveRecord::Migration[7.0]
  def change
    create_table :payments do |t|
      t.references :order, null: true, foreign_key: true
      t.references :customer, null: true, foreign_key: true
      t.integer :payment_gateway, null: false

      t.timestamps
    end
  end
end
