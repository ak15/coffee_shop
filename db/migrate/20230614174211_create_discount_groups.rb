class CreateDiscountGroups < ActiveRecord::Migration[7.0]
  def change
    create_table :discount_groups do |t|
      t.references :discount, null: true, foreign_key: true
      t.references :product, null: true, foreign_key: true

      t.timestamps
    end
  end
end
