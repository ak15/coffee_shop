class CreateDiscounts < ActiveRecord::Migration[7.0]
  def change
    create_table :discounts do |t|
      t.references :product, null: false, foreign_key: true
      t.decimal :discount, precision: 5, scale: 2, default: 0
      t.integer :category, null: false
      t.timestamps
    end
  end
end
