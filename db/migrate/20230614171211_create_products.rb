class CreateProducts < ActiveRecord::Migration[7.0]
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.decimal :price, precision: 10, scale: 2, default: 0
      t.decimal :tax, precision: 5, scale: 2, default: 0
      t.timestamps
    end
  end
end
