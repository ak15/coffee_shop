# CoffeeShop

## Table of Contents

- [Requirements](##requirements)
- [Initialization](##initialization)
- [Setup](##setup)
- [Deployment](##deployment)
- [Supplement](##supplement)

## Requirements

- Docker 19.x

If you run the project locally, the followings are required.

- Ruby 3.0.0
- Bundler 2.1.x
- Node.js 14.15.1
- Yarn 1.22.x
- Postgres 14.x

## Setup

Setup procedure of development environment.

Run `cp env.example .env` and open `.env` file to edit environment variables.

### Docker environment

Build docker containers

```bash
docker-compose build
```

Setup database Development

```bash
docker-compose run web bundle exec rake db:create db:migrate db:seed
```

Setup database Test

```bash
docker-compose run web bundle exec rake db:create db:migrate db:seed RAILS_ENV=test
```

Rswag

```bash
docker-compose run web  bundle exec rake rswag:specs:swaggerize
```

Start the app

```bash
docker-compose up
```

### Local environment without Docker

If you want to use Rails without Docker, then follow below steps

Install dependencies

```bash
bundle install
```

Start postgres and setup database

```bash
bin/rake db:create db:migrate db:seed
```

Start the app withot docker

```bash
bin/rails s
```

Start console

```bash
bin/rails c
```
