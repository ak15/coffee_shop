source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.0'

# Gems for all environments
gem 'active_storage_validations'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'devise'
gem 'devise-i18n'
gem 'devise-i18n-views'
gem 'doorkeeper'
gem 'dotenv-rails'
gem 'jbuilder', '~> 2.11.5'
gem 'multi_json'
gem 'nokogiri', '1.12.5'
gem 'pg', '~> 1.2'
gem 'puma', '~> 5.5.2'
gem 'rack-cors'
gem 'rails', '~> 7.0.1'
gem 'rails-i18n'
gem 'rswag', '~> 2.5.1'
gem 'sass-rails', '~> 5'
gem 'secure_headers'
gem 'slim'
gem 'turbo-rails'

group :production, :staging do
end

group :development, :test do
  gem 'brakeman'
  gem 'faker'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'pry-stack_explorer'
end
# since we need to run swaggerize command once in server (preview)
# https://github.com/rswag/rswag/issues/81#issuecomment-317534154
gem 'factory_bot_rails'
gem 'rspec'
gem 'rspec-rails', '~> 4.0.1'
gem 'shoulda-matchers'
# swaggerize does not run spec just needded for documentation updates
group :development do
  gem 'listen', '~> 3.4.1'
  gem 'rails-erd'
  gem 'rubocop'
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false
  gem 'spring', '~> 3.0.0'
  gem 'spring-commands-rspec'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'database_cleaner-active_record'
  gem 'selenium-webdriver'
  gem 'webdrivers'
end

gem 'tzinfo-data', platforms: %w[mingw mswin x64_mingw jruby]
